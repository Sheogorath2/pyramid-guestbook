from sqlalchemy import (
    Column,
    Integer,
    Text,
    Enum, Index)

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from pyramid.security import (
    Allow,
    Everyone,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

class Message(Base):
    __tablename__="messages"
    id = Column(Integer, primary_key=True)
    author = Column(Text)
    text = Column(Text)

class Permissions(Enum):
    edit = "edit"

class User(Base):
    __tablename__="users"
    id = Column(Integer, primary_key=True)
    name = Column(Text, unique=True)
    group = Column(Text)
    password = Column(Text)

class RootFactory(object):
    __acl__ = [(Allow, Everyone, 'view'),
                (Allow, 'group:editors', 'edit')]
    def __init__(self, request):
        pass

Index('my_index', User.name, unique=True, mysql_length=255)