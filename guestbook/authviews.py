from sqlite3 import IntegrityError

from pyramid.httpexceptions import HTTPFound
from pyramid.view import (
    view_config,
    forbidden_view_config,
    )

from pyramid.security import (
    remember,
    forget,
    )
from sqlalchemy.dialects import sqlite

from .models import (
    DBSession,
    Message,
    )

from .models import User
from .security import calculate_password_hash

@view_config(route_name='login', renderer='templates/login.mako')
@forbidden_view_config(renderer='templates/login.mako')
def login(request):
    login_url = request.route_url('login')
    referrer = request.url
    if referrer == login_url:
        referrer = request.route_url('home') # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)
    message = ''
    login = ''
    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        users=DBSession.query(User).filter_by(name=login)
        try:
            password_hash = calculate_password_hash(password)
            user_password_hash = users[0].password
            if password_hash == user_password_hash:
                headers = remember(request, login)
                return HTTPFound(location = came_from,
                                 headers = headers)
            message = 'Вы ввели неправильный пароль'
        except IndexError:
            message = 'Пользователя не существует'

    return dict(
        message = message,
        url = request.application_url + '/login',
        came_from = came_from,
        login = login,
        password = password,
        )

@view_config(route_name='logout')
def logout(request):
    headers = forget(request)
    return HTTPFound(location = request.route_url('home'),
                     headers = headers)

@view_config(route_name='signup',renderer='templates/login.mako')
def signup(request):
    reg_url = request.route_url('signup')
    referrer = request.url
    if referrer == reg_url:
        referrer = request.route_url('home') # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)
    message = ''
    login = ''
    password = ''
    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        user=User(name=login,password=calculate_password_hash(password),group="group:editors")
        message='Пользователь '+login+' уже есть!'
        if(DBSession.query(User).filter_by(name=user.name).count() == 0):
            DBSession.add(user)
            headers = remember(request, login)
            return HTTPFound(location = came_from,
                             headers = headers)

    return dict(
        message = message,
        url = request.application_url + '/signup',
        came_from = came_from,
        login = login,
        password = password,
        mode="signup"
        )