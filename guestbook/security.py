from sqlalchemy.exc import DBAPIError

from guestbook import DBSession
from guestbook.models import User

import hashlib

def groupfinder(userid,request):
    try:
        users=DBSession.query(User).filter_by(name=userid)
        #print(users[0].group)
        return [users[0].group]
    except (DBAPIError, IndexError):
        print("Ошибка с базой данных")
    return None

def calculate_password_hash(password):
    return hashlib.md5(bytes(password,'utf-8')).digest()