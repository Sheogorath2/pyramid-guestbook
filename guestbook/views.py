from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    Message,
    )

@view_config(route_name='home', renderer='templates/guestbook.mako')
def my_view(request):
    try:
        messages = DBSession.query(Message).all()
        print(request.authenticated_userid)
        if 'form.submitted' in request.params:
            author=request.authenticated_userid
            text=request.params['text']
            if (text and author):
                message=Message(author=author,text=text)
                DBSession.add(message)
                print(request.params)
                return HTTPFound()
    except DBAPIError:
        messages=[Message(author='',text='Ошибка с базой данных')]
        print("Ошибка с базой данных!")
    print(messages)
    messages.reverse()
    return dict(project="guestbook",
            messages=messages, logged_in=request.authenticated_userid)

@view_config(route_name='deletemessage', permission='edit')
def deletemessage(request):
    try:
        messages = DBSession.query(Message).filter_by(id=request.matchdict['messageid'])
        try:
            if(messages[0].author==request.authenticated_userid):
                DBSession.delete(messages[0])
            else:
                return Response('Вы должны зайти как '+messages[0].author)
        except IndexError:
            return Response('Сообщения '+request.matchdict['messageid']+' не существует')
    except DBAPIError:
        return Response('Ошибка с базой данных')
    #return Response('Message %(messageid)s deleted' % request.matchdict)
    return HTTPFound(location = request.route_url('home'))


