<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<body>
<link rel="stylesheet"
          href="${request.static_url('guestbook:static/formstyle.css') }"/>
<div class="centerdiv">
<font size="3" color="red">${message}</font>
<form action="${url}" method="post">
    <input type="hidden" name="came_from" value="${came_from}">
    <div class="form-group">
      <input type="text" name="login" value="${login}" placeholder="Пользователь" style="font-size:30px" >
    </div>
    <div class="form-group">
      <input type="password" name="password" value="${password}" placeholder="Пароль" style="font-size:30px">
    </div>
    <div class="form-group">
      <button class="mybtn" type="submit" name="form.submitted" value="Log In" class="btn btn-default" style="font-size:30px;float: inherit;">
        % if mode=="signup":
            Зарегистрироваться
        % else:
            Войти
        % endif
      </button>
    </div>
</form>
</div>
</body>
</html>