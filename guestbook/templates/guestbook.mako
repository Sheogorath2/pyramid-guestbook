<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<link rel="stylesheet"
          href="${request.static_url('guestbook:static/formstyle.css') }"/>
<link rel="import" src="${request.static_url('guestbook:static/meta.html')}">
<body>
% if logged_in != None:
    <form action="logout">
        Вы зашли как ${logged_in}
        <button class="mybtn" href="/logout">Выйти</button>
    </form>
    <form>
    ##     <input name="author" type="text" class="css-input" placeholder="ИМЯ" required><br>
    <textarea name="text" rows="4" cols="50" required class="css-input" placeholder="Ваш текст" autofocus></textarea>
    <br>
    <button class="mybtn" type="submit" name="form.submitted" value="Submit">ОТПРАВИТЬ</button>
    </form>
% else:
    <form action="${request.application_url}/login" style="margin: 0; padding: 0; display: inline;">
    <button class="mybtn">Войти</button>
    </form>
    <form action="${request.application_url}/signup" style="margin: 0; padding: 0; display: inline;">
    <button class="mybtn">Зарегистрироваться</button>
    </form>
% endif
<hr>
% if messages!=[]:
% for message in messages:
    <b>${message.author}</b> ${message.text}
    <a href="${request.application_url}/${message.id}/delete">
        % if message.author==logged_in:
            <img src="${request.static_url('guestbook:static/Delete_sign.png')}" height="15">
        % endif
    </a>
    <br>
% endfor
% endif
<script>var blurred = false;
window.onblur = function() { blurred = true; };
window.onfocus = function() { blurred && (location.reload()); };</script>
</body>
</html>